FROM yokohide0317/my-miniforge:1.0


RUN /root/miniconda3/bin/mamba create --name dgenies dgenies -y -c bioconda

Shell ["/bin/bash", "-c"]
WORKDIR /home

#RUN echo "conda activate dgenies" >> ~/.bashrc
RUN /root/miniconda3/bin/conda init

ENV CONDA_DEFAULT_ENV dgenies && \
    PATH /root/miniconda3/bin/envs/dgenies/bin:/root/miniconda3/bin:$PATH

WORKDIR /home

ADD requirements.txt /home/
SHELL ["/root/miniconda3/bin/mamba", "run", "-n", "dgenies", "/bin/bash", "-c"]
RUN python -m pip install -r requirements.txt

EXPOSE 5000

CMD ["/bin/bash"]
#SHELL ["conda", "init", "bash"]
CMD ["conda", "activate", "dgenies"]
CMD ["/root/miniconda3/envs/dgenies/bin/dgenies", "run", "--no-browser", "--host", "0.0.0.0"]
